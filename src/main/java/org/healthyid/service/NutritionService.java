package org.healthyid.service;

import org.healthyid.model.Nutrition;
import org.healthyid.repository.NutritionRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;

@ApplicationScoped
public class NutritionService {

    NutritionRepository nutritionRepository;

    @Inject
    public NutritionService(NutritionRepository nutritionRepository) {
        this.nutritionRepository = nutritionRepository;
    }

    @Transactional
    public Response addNutritionCatalog(Nutrition nutrition) {
        nutritionRepository.persist(nutrition);
        return Response.ok(nutrition).status(Response.Status.CREATED).build();
    }

}
