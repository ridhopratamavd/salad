package org.healthyid.service;

import org.healthyid.repository.FruitRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class FruitService {

    FruitRepository fruitRepository;

    @Inject
    public FruitService(FruitRepository fruitRepository) {
        this.fruitRepository = fruitRepository;
    }

    public String checkFruitAvailability(String name) {
        fruitRepository.findAll();
        return name.concat(" available");
    }
}
