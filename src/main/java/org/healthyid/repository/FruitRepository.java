package org.healthyid.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.healthyid.model.Fruit;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class FruitRepository implements PanacheRepository<Fruit> {
}
