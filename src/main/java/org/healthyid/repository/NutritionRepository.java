package org.healthyid.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.healthyid.model.Nutrition;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class NutritionRepository implements PanacheRepository<Nutrition> {
}
