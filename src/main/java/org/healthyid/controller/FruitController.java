package org.healthyid.controller;

import org.healthyid.service.FruitService;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/fruit")
@Produces(MediaType.APPLICATION_JSON)
public class FruitController {

    FruitService service;

    @Inject
    public FruitController(FruitService service) {
        this.service = service;
    }

    @GET
    public String getFruitAvailability() {
        return "hello";
    }

    @GET
    @Path("/{name}")
    public String getFruitAvailability(@PathParam String name) {
        return service.checkFruitAvailability(name);
    }

}
