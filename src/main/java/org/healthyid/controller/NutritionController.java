package org.healthyid.controller;

import org.healthyid.model.Nutrition;
import org.healthyid.service.NutritionService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/nutrition")
@Produces(MediaType.APPLICATION_JSON)
public class NutritionController {

    NutritionService nutritionService;

    @Inject
    public NutritionController(NutritionService nutritionService) {
        this.nutritionService = nutritionService;
    }

    @GET
    public String getNutrition() {
        return "hello";
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response add(Nutrition nutrition) {
        return nutritionService.addNutritionCatalog(nutrition);
    }
}
