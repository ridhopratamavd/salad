package org.healthyid;

import io.quarkus.test.junit.NativeImageTest;
import org.healthyid.controller.FruitControllerTest;

@NativeImageTest
public class NativeExampleResourceIT extends FruitControllerTest {

    // Execute the same tests but in native mode.
}
