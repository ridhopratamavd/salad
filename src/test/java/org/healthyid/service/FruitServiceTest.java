package org.healthyid.service;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertEquals;

@QuarkusTest
class FruitServiceTest {

    @Inject
    FruitService fuitService;

    @Test
    public void checkItemAvailability_shouldReturnConcatedantedResultWithAvaiiliable_whenInvoked() {
        String input = "apple";
        String expectedResult = input.concat(" available");

        final String actualResult = fuitService.checkFruitAvailability(input);

        assertEquals(expectedResult, actualResult);
    }
}
