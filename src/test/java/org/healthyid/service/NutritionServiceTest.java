package org.healthyid.service;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.test.junit.QuarkusTest;
import org.healthyid.model.Nutrition;
import org.healthyid.repository.NutritionRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;

import static org.junit.jupiter.api.Assertions.assertEquals;

@QuarkusTest
class NutritionServiceTest {

    @Inject
    NutritionService nutritionService;

    @Inject
    NutritionRepository nutritionRepository;

    @AfterEach
    @Transactional
    public void tearDown() {
        nutritionRepository.deleteAll();
    }

    @Test
    public void addNutritionCatalog_shouldSavetoDatabaseAndReturnItsObject_whenOnvoked() {
        Nutrition nutrition = Nutrition.builder()
                .name("nameee")
                .description("descriptionn")
                .build();

        final Response response = nutritionService.addNutritionCatalog(nutrition);
        final PanacheQuery<Nutrition> all = nutritionRepository.findAll();

        assertEquals(1, all.count());
        assertEquals(nutrition, response.getEntity());
        assertEquals(201, response.getStatus());
    }
}
