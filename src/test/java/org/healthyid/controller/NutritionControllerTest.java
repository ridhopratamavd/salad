package org.healthyid.controller;

import io.quarkus.test.junit.QuarkusTest;
import org.healthyid.model.Nutrition;
import org.healthyid.repository.NutritionRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.transaction.Transactional;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
class NutritionControllerTest {

    @Inject
    NutritionRepository nutritionRepository;

    @AfterEach
    @Transactional
    public void tearDown() {
        nutritionRepository.deleteAll();
    }

    @Test
    public void testHelloEndpoint() {
        Nutrition nutrition = Nutrition.builder()
                .description("make your skin fresh")
                .name("vitamin D")
                .build();

        given()
                .body(nutrition)
                .header("Content-Type", "application/json")
                .when()
                .post("/nutrition")
                .then()
                .statusCode(201)
                .body(is("{\"description\":\"make your skin fresh\",\"name\":\"vitamin D\"}"));
    }
}
