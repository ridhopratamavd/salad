package org.healthyid.controller;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class FruitControllerTest {

    @Test
    public void testHelloEndpoint() {
        given()
                .when().get("/fruit")
                .then()
                .statusCode(200)
                .body(is("hello"));
    }

    @Test
    public void testGreetingEndpoint() {
        String uuid = UUID.randomUUID().toString();
        given()
                .pathParam("name", uuid)
                .when().get("/fruit/{name}")
                .then()
                .statusCode(200)
                .body(is(uuid.concat(" available")));
    }

}
